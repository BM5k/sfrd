FROM ruby:alpine

# configure bundler
ENV BUNDLE_NO_PRUNE "true"
ENV BUNDLE_PATH     "/bundle/$RUBY_MAJOR"
ENV GEM_HOME        "$BUNDLE_PATH"
ENV BUNDLE_BIN      "$GEM_HOME/bin"

RUN apk upgrade --update && apk add --no-cache bash build-base less postgresql-dev ruby-dev sqlite-dev

RUN apk add

ENV app /src
RUN mkdir $app
WORKDIR $app
ADD . $app

ENTRYPOINT bash
