# frozen_string_literal: true
LOG_LEVEL = ENV.fetch('LOG_LEVEL') { 'warn' }

require 'bundler/inline'

gemfile true do
  source 'https://rubygems.org'

  gem 'rails'

  gem 'capybara'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'minispec-rails'
  gem 'pg'
  gem 'pry-rails'
  gem 'slim'
end

ties = %w[ active_record action_controller action_mailer active_model ]
ties.each { |t| require "#{ t }/railtie" }

class TestApp < Rails::Application
  config.consider_all_requests_local = true
  config.i18n.default_locale         = :en
  config.i18n.available_locales      = :en
  config.log_level                   = LOG_LEVEL
  config.logger                      = Logger.new STDOUT

  Rails.logger = config.logger
  ActiveRecord::Base.logger = config.logger
  ActiveRecord::Base.logger.level = LOG_LEVEL
  ActiveRecord::Migration.verbose = LOG_LEVEL == 'debug'

  routes.draw do
    resources :users
    root to: 'users#index'
  end
end

## Inline i18n config
# translations = I18n::Backend::KeyValue.new 'en.home' => '"Home"'
# I18n.backend = I18n::Backend::Chain.new(translations, I18n.backend)

db = {
  adapter:  'postgresql',
  database: 'rails_development',
  encoding: 'unicode',
  host:     'postgres',
  username: 'postgres'
}.with_indifferent_access

ActiveRecord::Base.establish_connection db

begin
  tasks = ActiveRecord::Tasks::DatabaseTasks
  adapter = tasks.send :class_for_adapter, db[:adapter]
  adapter.new(db).create
rescue ActiveRecord::Tasks::DatabaseAlreadyExists
  Rails.logger.debug 'Database Already Exists'
end

ActiveRecord::Schema.define do
  create_table :users, force: true do |t|
    t.string :username
    t.timestamps
  end
end

class User < ActiveRecord::Base
end

class UsersController < ActionController::Base
  include ActionView::RecordIdentifier

  def index
    @users = User.all

    slim = Slim::Template.new do
      <<~'SLIM'.gsub '\n', "\n"
        link[
          crossorigin="anonymous"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          rel="stylesheet"]

        .container
          h1 = User.model_name.human.pluralize
          table#users.table.table-bordered.table-hover.table-striped
            thead
              tr
                th= User.human_attribute_name :username
                th= User.human_attribute_name :updated_at

            tbody
              - for user in @users
                tr id=dom_id(user)
                  td= user.username
                  td #{ helpers.time_ago_in_words user.updated_at } ago
      SLIM
    end

    render inline: slim.render(self)
  end
end

FactoryBot.define do
  factory :user do
    username { Faker::Internet.username }
  end
end

require 'minitest/spec'

Minitest.module_eval "@@installed_at_exit = true" # prevent autorun

require 'rails/test_help'
require 'minispec/rails/system_test'
require 'capybara/minitest/spec'

class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods
  fixtures :all
end

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include ActionView::Helpers::DateHelper
  include ActionView::RecordIdentifier
  include Rails.application.routes.url_helpers

  driven_by :rack_test, screen_size: [1400, 900]
end

describe User do
  before do
    @user = create :user
  end

  it 'has a username' do
    @user.username.must_be :present?
  end
end

describe 'User system' do
  before do
    @users = create_list :user, 3
    visit users_path
  end

  it 'includes each user' do
    within('body') { page.must_have_content 'Users' }

    within '#users' do
      @users.each do |user|
        within("##{ dom_id user }") do
          page.must_have_content user.username
          page.must_have_content time_ago_in_words(user.updated_at)
        end
      end
    end
  end
end

exit_code = Minitest.run
exit exit_code unless exit_code

unless ENV['SERVER'] == 'false'
  FactoryBot.create_list :user, 10 unless User.any?

  require 'rails/commands/server/server_command'

  Rails::Server.new(app: TestApp, Host: '0.0.0.0', Port: 3000).start
end
