# Single File Rails App

Run a tiny rails app with docker-compose

```
$ git clone git@gitlab.com:BM5k/sfrd.git
$ cd sfrd
$ docker-compose build rails
$ docker-compose run rails
```

See also
- https://christoph.luppri.ch/articles/rails/single-file-rails-applications-serving-requests/
- https://gist.github.com/dgb/1383463
- https://www.amberbit.com/blog/2014/2/14/putting-ruby-on-rails-on-a-diet/#single-file-rails-applications
